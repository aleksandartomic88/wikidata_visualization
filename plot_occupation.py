# -*- coding: utf-8 -*-
"""
Created on Sat Nov  7 09:56:48 2020

@author: Aleksandar
"""

import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import matplotlib.animation as animation
from IPython.display import HTML
import random

number_of_colors = 50

color = ["#"+''.join([random.choice('0123456789ABCDEF') for j in range(6)])
             for i in range(number_of_colors)]

groups = ["G" + str(x) for x in range(1, number_of_colors)]

colors = dict(zip(
    groups,
    color
))

group_lk = dict(zip(
    ["astronomer", "rabbi", "soldier", "priest", "physician", "Catholic priest", "military officer",
     "writer", "military personnel", "politician", "painter", "Lawyer", "architect", "bushi",
     "aristocrat", "composer", "theologian", "muhaddith", "ruler", "diplomat", "military commander",
     "historian", "sovereign", "monarch", "philosopher", "monk", "military leader", "catholic priest",
     "laywer", "Painter", "journalist", "actor", "poet", "laywer", "association football player", "university teacher", "lawyer"],
    groups
    ))

int_to_roman = dict(zip(
    list(range(1, 21)),
    ["I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X", "XI", "XII", "XIII", "XIV", "XV", "XVI", "XVII", "XVIII", "XIX", "XX"]
    ))

import pyodbc
cnxn = # removed connection to my server...


def get_data_for_century(century):
    df = pd.read_sql_query('''
select TOP 10 count(*) as cnt, LOWER(value) as occupation
from openrowset(bulk 'https://atomicstorageaccount.blob.core.windows.net/wikidata/*people*.parquet', format='parquet') as p
OUTER APPLY 
OPENJSON (p.occupation)
where value is not null and (date_of_death is not null or date_of_death is not null)
and ((DATEPART(year, date_of_death) / 100 + 1) = {0} or (DATEPART(year, date_of_birth) / 100 + 1) = {0})
GROUP BY LOWER(value)
ORDER BY cnt DESC
'''.format(century), cnxn)
    return df

def draw_barchart(current_year):
    print(current_year)
    dff = get_data_for_century(current_year).sort_values(by='cnt', ascending=True)
    ax.clear()
    ax.barh(dff['occupation'], dff['cnt'], color=[colors[group_lk[x]] for x in dff['occupation']])
    #dx = dff['cnt'].max()
    #for i, (value, name) in enumerate(zip(dff['cnt'], dff['occupation'])):
    #    ax.text(value-dx, i,     name,           size=16, weight=600, ha='right', va='bottom')
    #    ax.text(value-dx, i-.25, group_lk[name], size=10, color='#444444', ha='right', va='baseline')
    #    ax.text(value+dx, i,     f'{value:,.0f}',  size=14, ha='left',  va='center', color='blue', fontweight='bold')
    ax.text(1, 0.4, int_to_roman[current_year] + " Century", transform=ax.transAxes, color='#777777', size=46, ha='right', weight=800)
    #ax.text(0, 1.06, 'Num of people', transform=ax.transAxes, size=12, color='#777777')
    ax.xaxis.set_major_formatter(ticker.StrMethodFormatter('{x:,.0f}'))
    ax.xaxis.set_ticks_position('bottom')
    #ax.tick_params(axis='x', colors='#777777', labelsize=12)
    #ax.set_yticks([])
    ax.margins(0, 0.01)
    ax.grid(which='major', axis='x', linestyle='-')
    ax.set_axisbelow(True)
    ax.text(0, 1.15, 'The most common occupations \n of people on Wikipedia per century AD',
            transform=ax.transAxes, size=20, weight=600, ha='left', va='top')
    plt.box(False)
    #plt.savefig("C:\\Users\\bmark\\Documents\\curr_year{0}.png".format(current_year))
    #plt.show()
    
for x in range(1, 21):
    draw_barchart(x)    
    
fig, ax = plt.subplots(figsize=(15, 8))
animator = animation.FuncAnimation(fig, draw_barchart, frames=range(1, 21))

from matplotlib.animation import FuncAnimation, PillowWriter 
writer = PillowWriter(fps=0.6)  
animator.save("C:\\users\\bmark\\desktop\occupation_all_med.gif", writer=writer)  

# used ffmpeg to convert to video
# ffmpeg -i occupation_all_med.gif -movflags faststart -pix_fmt yuv420p -vf "scale=trunc(iw/2)*2:trunc(ih/2)*2" video.mp4