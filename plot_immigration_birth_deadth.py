
from mpl_toolkits.basemap import Basemap
import numpy as np
import matplotlib.pyplot as plt
import math

countries = ['India', 'Brazil', '''People''s Republic of China''', 'Japan', 'Indonesia', 'Democratic Republic of the Congo', 'Mexico']

fig=plt.figure(figsize=(20, 15 * len(countries)))
subplot_index = len(countries) * 100 + 10 + 1

for c in countries:
    ax = fig.add_subplot(subplot_index)
    subplot_index += 1
    ax.set_title(c, fontsize=20)
    m = Basemap(projection='cyl', lon_0=0, resolution='c')
    
    m.fillcontinents(color='coral',lake_color='aqua')
    m.drawmapboundary(fill_color='aqua')
    markers = get_markers(c)
    markers.to_csv("C:\\Users\\bmark\\Documents\\{0}.csv".format(c), sep=',')
    lon1, lat1 = m(markers.birth_lon.values, markers.birth_lat.values)
    lon2, lat2 = m(markers.death_lon.values, markers.death_lat.values)
    
    pts = np.c_[lon1, lat1, lon2, lat2].reshape(len(lon1), 2, 2)
    
    for ptPair in pts:
        lo1, la1 = ptPair[0]
        lo2, la2 = ptPair[1]
        if (math.isnan(lo1) or math.isnan(la1) or math.isnan(lo2) or math.isnan(la2)):
            continue
        line, = m.drawgreatcircle(lo1, la1, lo2, la2, lw=3)
    
        p = line.get_path()
        # find the index which crosses the dateline (the delta is large)
        cut_point = np.where(np.abs(np.diff(p.vertices[:, 0])) > 200)[0]
        if cut_point:
            cut_point = cut_point[0]
    
            # create new vertices with a nan inbetween and set those as the path's vertices
            new_verts = np.concatenate(
                                       [p.vertices[:cut_point, :], 
                                        [[np.nan, np.nan]], 
                                        p.vertices[cut_point+1:, :]]
                                       )
            p.codes = None
            p.vertices = new_verts
    
    
plt.title('Immigration of notable people')
plt.savefig("all.png".format(c))
plt.show()